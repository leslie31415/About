![CEADS](/CEADS_logo_DV_light.png)

# Dr. Leslie Kerby 
_Assistant Professor_   
Informatics and Computer Science  
_Affiliate Faculty_  
Nuclear Engineering and Health Physics  
Idaho State University

**[CV](https://gitlab.com/CEADS/DrKerby/CV/blob/master/Kerby_CV.pdf)**

My research interests are interdisciplinary and include data science, machine learning, AI, computational science, and nuclear science and engineering. I am director of Computational Engineering And Data Science (CEADS). 

CEADS research group accomplishments over the past 4 years at ISU: 
- Almost $1 million in external funding from half a dozen contracts with Idaho National Laboratory, Oak Ridge National Laboratory, and Los Alamos National Laboratory
- Mentored a dozen students, including major advisor for four PhD students and several MS students
- Two dozen peer-reviewed publications
- Developed and taught Applied Neural Networks, Data Mining and Predictive Analytics, Computational Engineering in C+, and Modern Nuclear Computing; taught Data Structures and Algorithms, Introduction to Python, Introduction to Informatics and Analytics, and Monte Carlo Methods and Applications
- Gave one-day "Introduction to Data Science" workshop in CAES May 2019 with INL collaborator Josh Peterson; developed two-day "Introduction to Data Science and Machine Learning" workshop given in August 2019
- Invited speaker, "CEM and LAQGSM Event Generators: Modern Software Development", at the Cross Sections for Cosmic Rays @ CERN (XSCRC2019), November 2019 in CERN, Geneva, Switzerland
- Invited workshop, "Introduction to Data Science with Python", at the American Nuclear Society Student Conference, April 2019 in Richmond, VA  
- Invited member of panel, "Thermal Hydraulics Applications of Machine Learning and Data Science", at the American Nuclear Society Winter Meeting, November 2018 in Orlando, FL
- First Place in the IEEE Big Data and IEEE Brain Hackathon, July 2018 in Tokyo, Japan
- First Place Best Overall Paper at the American Nuclear Society Student Conference, April 2017 in Pittsburgh, PA

Current research includes
- Utilizing machine learning to recognize nuclear reactor transients and provide information to operators in real-time
- Novel methods for data storage and transfer within computational and multiphysics codes
- Database methods for nuclear materials
- Modernization of and development of APIs for legacy codes
- Multiphysics coupling drivers and APIs

**[Data Science Notebooks from Workshop](https://gitlab.com/CEADS/DrKerby/python/)**

**[CAES 10th Anniversary](https://ginaborudphotography.shootproof.com/gallery/caes2019/home)** \
&emsp; CEADS students presented posters: \
&emsp; &emsp;  Chase Juneau, 'An Overview of the Generalized Spallation Model' \
&emsp; &emsp; Pepo Mena, 'Reactor Transient Classification Using Machine Learning'

**Publications** \
[Volumetric Spherical Polynomials](https://doi.org/10.1063/1.5086695)

[Financial Analysis of Modular Nuclear Reactors](http://www.ansannual.org/trans/2019/data/pdfs/347-29707.pdf)

[Weighted Delta-Tracking in Scattering Media](https://www.sciencedirect.com/science/article/pii/S0029549318310239)

_More Coming soon..._  

**Classes Taught**  
[Applied Neural Networks](https://elearn.isu.edu/moodle/course/view.php?id=47393), _Spring 2020_  
[Data Structures and Algorithms](https://elearn.isu.edu/moodle/course/view.php?id=47392), _Spring 2020_  
[Data Mining and Predictive Analytics](https://elearn.isu.edu/moodle/course/view.php?id=41182), _Fall 2019_  
[Introduction to Programming with Python](https://elearn.isu.edu/moodle/course/view.php?id=41180#section-0), _Fall 2019_  
Computational Engineering with C++, _Spring 2019, Fall 2017_  
Introduction to Informatics and Analytics, _Spring 2019_  
Monte Carlo Methods and Applications, _Fall 2018, Spring 2018, Spring 2017_  
Modern Nuclear Computing, _Fall 2016_  

**Other**  
_Co-Lead_, CAES Computing, Data, and Visualization \
_Lead_, ISU Data Science Alliance \
_Reviewer_, Nuclear Technology \
_Reviewer_, IEEE Transactions on Nuclear Science \
_Reviewer_, ANS M\&C \
_Reviewer_, Nuclear Science and Engineering 

**Student Theses**  
Brycen Wendt, PhD \
[Functional Expansions Methods: Optimizations, Characterizations,and Multiphysics Practices](https://github.com/LGKerby/Docs/blob/master/WendtDissertationFinal.pdf)

Chase Juneau, MS \
[The Development of the Generalized Spallation Model](https://github.com/LGKerby/Docs/blob/master/gsmThesisFinal.pdf)  

Pedro Mena, MS \
[Reactor Transient Classification Using Machine Learning](https://github.com/LGKerby/Docs/blob/master/Mena_Pedro_MS_Final.pdf)  


